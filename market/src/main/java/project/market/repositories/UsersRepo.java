package project.market.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import project.market.models.Users;

@Repository
public interface UsersRepo extends JpaRepository<Users, Long>{
	@Query(value="SELECT * FROM users u WHERE u.username= ?1 AND u.pass = ?2", nativeQuery=true)
	List<Users> getLogin(String username, String pass);
}
